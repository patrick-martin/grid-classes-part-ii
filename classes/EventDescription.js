class EventDescription {

    constructor({ listener, type, callback, context = null }) {
        this.listener = listener
        this.type = type
        this.callback = (context) ? callback.bind(context) : callback

    }
}



// //const eventListeners = [
//     new EventDescription({
//         listener: gridElement,
//         type: 'click',
//         callback: function (event) {
//           console.log('`this` means', grid)
//           console.log('A click has been detected on', event.currentTarget)
//         },
//         context: grid
//       })
//     ]

//     grid.addEventListeners(eventListeners)


// class EventDescription {

//     constructor ({ listener, type, callback, context = null }) {
//       this.listener = listener
//       this.type = type
//       this.callback = context ? callback.bind(context) : callback
//     }

//   }