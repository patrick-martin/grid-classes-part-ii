class TicTacToeGrid extends GenericGrid {

    constructor(options) {
        super(options)
        this.playerTurn = 'black'

    }

    togglePlayerTurn() {
        this.playerTurn = (this.playerTurn === 'black') ?
            'red' :
            'black'
    }

    createCell() {
        return new TicTacToeCell(this.options)
    }

    registerPlayerOnCell(event) {
        console.log(event.target)
        const cellElement = event.target
        const cell = cellElement.instance
        cell.registerPlayerOwnership(this.playerTurn)

        this.togglePlayerTurn()


        // if (this.playerTurn === 'X') {
        //     event.target.style.backgroundColor = 'black'
        //     this.togglePlayerTurn()
        // } else {
        //     event.target.style.backgroundColor = 'red'
        //     this.togglePlayerTurn()
        // }
    }

}

// if (this.playerTurn === 'X') {
//     event.target.style.backgroundColor = 'black'
//     this.togglePlayerTurn()
// } else {
//     event.target.style.backgroundColor = 'red'
//     this.togglePlayerTurn()
// }