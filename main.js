const gridContainer = document.getElementById('grid-container')

const grid = new TicTacToeGrid({
    rowCount: 3,
    columnCount: 3,
    cellHeight: '120px',
    cellWidth: '120px',
    cellClasses: ['red'],
    cellType: TicTacToeCell,
})

const gridElement = grid.createGridElement()
gridContainer.appendChild(gridElement)

const eventListeners = [
    new EventDescription({
        listener: gridElement,
        type: 'click',
        // callback: function(event) {
        //     console.log(event.currentTarget) //returns Object HTMLDivElement.  currentTarget is what the event is attached to
        //     console.log(event.target) //event.target is what is clicked on (child element of parent element which has event attached to it)
        // },
        callback: grid.registerPlayerOnCell,
        context: grid,
    })
]
grid.addEventListeners(eventListeners)


// //const eventListeners = [
//     new EventDescription({
//         listener: gridElement,
//         type: 'click',
//         callback: function (event) {
//           console.log('`this` means', grid)
//           console.log('A click has been detected on', event.currentTarget)
//         },
//         context: grid
//       })
//     ]

//     grid.addEventListeners(eventListeners)





// const eventListeners = [{
//         listener: document,
//         eventType: 'keydown',
//         callback: function(event) { console.log('A key was pressed and it was ' + event.key) }
//     },
//     {
//         listener: grid,
//         eventType: 'click',
//         callback: function(event) { console.log('An element was clicked and it was ' + grid) }
//     },
//     {
//         listener: grid,
//         eventType: 'hover',
//         callback: function(event) { console.log('You hovered over the ' + grid + ' element.') }
//     }

// ]